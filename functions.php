<?php

function load_stylesheets(){
    
    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), false, 'all');
    wp_enqueue_style('bootstrap');
    
    wp_register_style('main', get_template_directory_uri() . '/css/main.css', array(), false, 'all');
    wp_enqueue_style('main');
    
}
add_action('wp_enqueue_scripts', 'load_stylesheets');

function include_jquery(){
    
    wp_deregister_script('jquery');
    
    wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery-3.4.1.min.js', '', 1, true);
    
    add_action('wp_enqueue_scripts', 'jquery');
    
}
add_action('wp_enqueue_scripts', 'include_jquery');

function loadjs(){
    
    wp_register_script('customjs', get_template_directory_uri() . '/js/scripts.js', '', 1, true);
    wp_enqueue_script('customjs');
    
}
add_action('wp_enqueue_scripts', 'loadjs');

add_theme_support('menus');
add_theme_support('post-thumbnails');
add_theme_support('widgets');

register_nav_menus(
    array(
        'top-menu' => __('Top Menu', 'theme'),
        'footer-menu' => __('Footer Menu', 'theme'),
    )
);

add_image_size('smallest', 200, 250, true);
add_image_size('middle', 243, 304, true);
add_image_size('largest', 1110, 250, array('center', 'top'));


function my_sidebars(){

    register_sidebar(
        array(
            'name' => 'Page Sidebar',
            'id' => 'page-sidebar',
            'before_title' => '<h4 class="widget-title">',
            'after_title' => '</h4>'
        )
    );

    register_sidebar(
        array(
            'name' => 'Blog Sidebar',
            'id' => 'blog-sidebar',
            'before_title' => '<h4 class="widget-title">',
            'after_title' => '</h4>'
        )
    );

} 
add_action('widgets_init', 'my_sidebars');


// Remove pages from search results
function exclude_pages_from_search($query) {
    if ( $query->is_main_query() && is_search() ) {
        $query->set( 'post_type', 'post' );
    }
    return $query;
}
add_filter( 'pre_get_posts','exclude_pages_from_search' );


function my_first_post_type(){

    $args = array(

        'labels' => array(
            'name' => 'Cars',
            'singular_name' => 'Car',
        ),
        'hierarchical'=> true,
        'public' => true,
        'has_archive' => true,
        'menu_icon' => 'dashicons-marker',
        'supports' => array('title', 'editor', 'thumbnail', 'custom-fields'),
    );

    register_post_type('cars', $args);

}
add_action('init', 'my_first_post_type');

function my_first_taxonomy(){

    $args = array(
        'labels' => array(
            'name' => 'Brands',
            'singular_name' => 'Brand',
        ),
        'public' => true,
        'hierarchical'=> false,
    );
    register_taxonomy('brands', array('cars'), $args);

}
add_action('init', 'my_first_taxonomy');


//Logo
add_theme_support( 'custom-logo', array(
    'height'      => 100,
    'width'       => 400,
    'flex-height' => false,
    'flex-width'  => false,
    'header-text' => array( 'site-title', 'site-description' ),
) );

//Customizer file
require get_template_directory(). '/inc/customizer.php';

/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */

function wpdocs_custom_excerpt_length( $length ) {
    return 12;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );
