<?php get_header(); ?>
<div id="blog_dots">
<div class="container" id="blog_container">
    <div id="section_blog">
        <div id="blog_section">
            <div id="blog_wrapper">
                <?php  get_template_part('includes/section', 'archive'); ?>
                <div class="row mb-2">
                <div class="col-md-3">
                    <div id="blog_pagination">
                        <?php
                            global $wp_query;

                            $big = 999999999; //need an unlikely integer

                            echo paginate_links(array(
                                'base' => str_replace($big, '%#%', esc_url(get_pagenum_link( $big ) ) ),
                                'format' => '?paged=%#%',
                                'current' => max(1, get_query_var('paged') ),
                                'total' => $wp_query->max_num_pages
                            ));
                        ?>
                    </div>
                </div>
            </div>
            </div> 
            
            <!--div id="blog_pagination">
                <?php

                global $wp_query;

                $big = 999999999; //need an unlikely integer

                echo paginate_links(array(
                    'base' => str_replace($big, '%#%', esc_url(get_pagenum_link( $big ) ) ),
                    'format' => '?paged=%#%',
                    'current' => max(1, get_query_var('paged') ),
                    'total' => $wp_query->max_num_pages
                ));

                ?>
            </div-->           
        </div>
    </div>
</div>
</div>


<?php get_footer(); ?>