<?php
    function nippon_customize_register($wp_customize){
        //Showcase Section
        $wp_customize->add_section('showcase', array(
            'title' => __('Showcase', 'wpbootstrap'),
            'description' => sprintf(__('Options for showcase', 'wpbootstrap')),
            'priority' => 130
        ));


        //Images and text
        //Main image
        $wp_customize->add_setting('showcase_image', array(
            'default' => get_bloginfo('template_directory').'/img/totoro-rain.jpg',
            'type' => 'theme_mod'
        ));

        $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'showcase_image', array(
            'label' => __('Showcase Image', 'wpbootstrap'),
            'section' => 'showcase',
            'settings' => 'showcase_image',
            'priority' => 1    
        )));
        //And text
        $wp_customize->add_setting('showcase_heading', array(
            'default' => _x('Lorem Ipsum Dolor Sit Amet', 'nippon'),
            'type' => 'theme_mod'
        ));

        $wp_customize->add_control('showcase_heading', array(
            'label' => __('Heading', 'nippon'),
            'section' => 'showcase',
            'priority' => 2
        ));


        //Next image
        $wp_customize->add_setting('showcase_break1', array(
            'default' => get_bloginfo('template_directory').'/img/akira-smoke.jpg',
            'type' => 'theme_mod'
        ));

        $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'showcase_break1', array(
            'label' => __('Break Image 1', 'nippon'),
            'section' => 'showcase',
            'settings' => 'showcase_break1',
            'priority' => 3    
        )));
        //And text
        $wp_customize->add_setting('showcase_textbr1', array(
            'default' => _x('In nomine Patris et Filii et Spiritus Sancti. Amen', 'nippon'),
            'type' => 'theme_mod'
        ));

        $wp_customize->add_control('showcase_textbr1', array(
            'label' => __('Text Break 1', 'nippon'),
            'section' => 'showcase',
            'priority' => 4
        ));


        //etc.
        $wp_customize->add_setting('showcase_break2', array(
            'default' => get_bloginfo('template_directory').'/img/your-name-short.jpg',
            'type' => 'theme_mod'
        ));

        $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'showcase_break2', array(
            'label' => __('Break Image 2', 'nippon'),
            'section' => 'showcase',
            'settings' => 'showcase_break2',
            'priority' => 5    
        )));

        $wp_customize->add_setting('showcase_textbr2', array(
            'default' => _x('In nomine Patris et Filii et Spiritus Sancti. Amen', 'nippon'),
            'type' => 'theme_mod'
        ));

        $wp_customize->add_control('showcase_textbr2', array(
            'label' => __('Text Break 2', 'nippon'),
            'section' => 'showcase',
            'priority' => 6
        ));


        //etc.
        $wp_customize->add_setting('showcase_socials', array(
            'default' => get_bloginfo('template_directory').'/img/totoro-fishing.jpg',
            'type' => 'theme_mod'
        ));

        $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'showcase_socials', array(
            'label' => __('Socials Image ', 'nippon'),
            'section' => 'showcase',
            'settings' => 'showcase_socials',
            'priority' => 7    
        )));

        $wp_customize->add_setting('showcase_textsocials', array(
            'default' => _x('In nomine Patris et Filii et Spiritus Sancti. Amen', 'nippon'),
            'type' => 'theme_mod'
        ));

        $wp_customize->add_control('showcase_textsocials', array(
            'label' => __('Text Socials', 'nippon'),
            'section' => 'showcase',
            'priority' => 8
        ));

        
        //Color changes
        $wp_customize->add_section('custom_colors', array(
            'title' => __('Custom Colors', 'Nippon'),
            'priority' => 140
        ));

        $wp_customize->add_setting('background_color', array(
            'default' => 'af001a',
            'transport' => 'refresh'
        ));

        $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'background_color_control', array(
            'label' => __('Background Color', 'Nippon'),
            'section' => 'custom_colors',
            'settings' => 'background_color',
            'priority' => 1
        ) ) );

        $wp_customize->add_setting('logo_section_color', array(
            'default' => '#af001a',
            'transport' => 'refresh'
        ));

        $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'logo_section_color_control', array(
            'label' => __('Logo Section Color', 'Nippon'),
            'section' => 'custom_colors',
            'settings' => 'logo_section_color',
            'priority' => 2
        ) ) );

        $wp_customize->add_setting('header_color', array(
            'default' => '#3d000a',
            'transport' => 'refresh'
        ));

        $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header_color_control', array(
            'label' => __('Header Color', 'Nippon'),
            'section' => 'custom_colors',
            'settings' => 'header_color',
            'priority' => 3
        ) ) );

        $wp_customize->add_setting('breaks_color', array(
            'default' => '#3d000a',
            'transport' => 'refresh'
        ));

        $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'breaks_color_control', array(
            'label' => __('Breaks Color', 'Nippon'),
            'section' => 'custom_colors',
            'settings' => 'breaks_color',
            'priority' => 4
        ) ) );

        $wp_customize->add_setting('page_color', array(
            'default' => '#ffffff',
            'transport' => 'refresh'
        ));

        $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_color_control', array(
            'label' => __('Page Color', 'Nippon'),
            'section' => 'custom_colors',
            'settings' => 'page_color',
            'priority' => 5
        ) ) );

        $wp_customize->add_setting('socials_page_color', array(
            'default' => '#ffffff',
            'transport' => 'refresh'
        ));

        $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'socials_page_color_control', array(
            'label' => __('Social Media Page Color', 'Nippon'),
            'section' => 'custom_colors',
            'settings' => 'socials_page_color',
            'priority' => 6
        ) ) );

        $wp_customize->add_setting('social_icons_color', array(
            'default' => '#bc002d',
            'transport' => 'refresh'
        ));

        /*$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'social_icons_color_control', array(
            'label' => __('Social Media Icons Color', 'Nippon'),
            'section' => 'custom_colors',
            'settings' => 'social_icons_color',
            'priority' => 7
        ) ) );*/

        $wp_customize->add_setting('footer_color', array(
            'default' => '#3d000a',
            'transport' => 'refresh'
        ));

        $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_color_control', array(
            'label' => __('Footer Color', 'Nippon'),
            'section' => 'custom_colors',
            'settings' => 'footer_color',
            'priority' => 8
        ) ) );
        
    }

    add_action('customize_register', 'nippon_customize_register');
