<?php get_header(); ?>

<div id="home">
    <div class="col col-lg-6">
        <h1><?php echo get_theme_mod('showcase_heading'); ?></h1>
    </div>
</div>

<div class="break"></div>

<div class="section" id="section1">
    <div id="container" class="container" style="background-color: white; heigt: 100%;">
        <div class="home-text">
            <div class="empty">
                <img src="<?php bloginfo('template_directory'); ?>/img/man-holding-smartphone.jpg" />
            </div>
            <div class="content">
                <div class="title">
                    <h1><?php the_title(); ?></h1>
                </div>
                <div class="about">
                    <?php get_template_part('includes/section', 'content'); ?>
                </div>
            </div>
        </div>
        <div class="image">
            <p>We worked on such projects as:</p>
            <ul>
                <li>Akira</li>
                <li>Nichijou</li>
                <li>One Punch Man</li>
                <li>Epstein Didn't Kill Himself</li>
                <li>My Neighbour Totoro</li>
            </ul>
            <p>We have:</p>
            <ul>
                <li>Sold over 3000 copies of our comics</li>
                <li>Been in business for 12 years</li>
                <li>Won 2012 KIKIKOKO award for best comic</li>
                <li>Won 2008 POLOJO award for best artstyle</li>
            </ul>
            <p>We have experience in:</p>
            <ul>
                <li>Glanao</li>
                <li>Rebere</li>
                <li>KenoKenoKonichiua</li>
            </ul>
        </div>
    </div>
</div>

<div class="break"></div>

<div id="window">
    <div class="col col-lg-4">
        <h1><?php echo get_theme_mod('showcase_textbr1'); ?></h1>
    </div>
</div>

<div class="break"></div>

<div class="section" id="section2">
    <div class="curtain"></div>
    <div id="container_middle" class="container" style="background-color: white; heigt: 100%;">
        <div class="main">
            <div class="text"><h1>Visit Our Library</h1></div>
            <div class="center">
                <a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/saitama-japan-symbol.png" /></a>
            </div>
            <div class="text"><h1>And See Our Work</h1></div>
        </div>
    </div>
    <div class="curtain"></div>
</div>

<div class="break"></div>

<div id="window2">
    <div class="col col-lg-4">
        <h1><?php echo get_theme_mod('showcase_textbr2'); ?></h1>
    </div>
</div>

<div class="break"></div>

<div class="section" id="section3">
    <div id="container-last" class="container" style="background-color: white; heigt: 100%;">
        <div class="text"><h1>Visit Us On<br/>Social Media</h1></div>
            <div class="center">
                <div class="circle" id="inst"><i class="fab fa-instagram"></i></div>
                <div class="circle" id="face"><i class="fab fa-facebook-f"></i></div>
            </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.circle#inst').on('mouseenter', function(){
            $('.circle#inst i').css('color', 'white').fadeIn(4000);
        });
        $('.circle#inst').on('mouseleave', function(){
            $('.circle#inst i').css('color', 'black').fadeIn(4000);
        });
        $('.circle#face').on('mouseenter', function(){
            $('.circle#face i').css('color', 'white').fadeIn(4000);
        });
        $('.circle#face').on('mouseleave', function(){
            $('.circle#face i').css('color', 'black').fadeIn(4000);
        });
    });
</script>

<?php get_footer(); ?>