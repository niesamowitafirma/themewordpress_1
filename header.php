<!doctype html>
<html lang="en">
  <head>
    <?php wp_head(); ?>  
    
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Blog Template · Bootstrap</title>

    <!--Font-->
    <link href="https://fonts.googleapis.com/css?family=Big+Shoulders+Text:400,700&display=swap" rel="stylesheet">


    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/blog/">

    <!-- Bootstrap core CSS -->
    <link href="/docs/4.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      .section #container{
        background-color: <?php echo get_theme_mod('page_color'); ?>!important;
      }
      
      #social_1, 
      #social_3{
        background-color: <?php echo get_theme_mod('socials_page_color'); ?>;
      }

      /*.circle{
        background-color: <?php echo get_theme_mod('social_icons_color'); ?>;
      }*/

      #header{
        background-color: <?php echo get_theme_mod('header_color'); ?>;
      }

      #home{
        background: url(<?php echo get_theme_mod('showcase_image', get_bloginfo('template_url').'/img/totoro-rain.jpg'); ?>) no-repeat center center fixed;
      }

      #window{
        background: url(<?php echo get_theme_mod('showcase_break1', get_bloginfo('template_url').'/img/akira-smoke.jpg'); ?>) no-repeat center center;
      } 

      #window2{
        background: url(<?php echo get_theme_mod('showcase_break2', get_bloginfo('template_url').'/img/your-name-short.jpg'); ?>) no-repeat center center;
      }

      #image_2{
        background: url(<?php echo get_theme_mod('showcase_socials', get_bloginfo('template_url').'/img/totoro-fishing.jpg'); ?>) no-repeat center center;
      }

      #logo_section{
        width: 100%; 
        max-width: 9000px;
        background-color: <?php echo get_theme_mod('logo_section_color'); ?>; 
        padding: 0;
      }

      .break{
        background-color: <?php echo get_theme_mod('breaks_color'); ?>; 
      }

      .footer{
        background-color: <?php echo get_theme_mod('footer_color'); ?>;
      }

      html, body{
        background-color: #<?php echo get_theme_mod('background_color'); ?>;
        margin: 0;
        height: 100%;
        width: 100%;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      
    </style>
    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="blog.css" rel="stylesheet">
 
   </head>
  <body <?php body_class(); ?>>
  <div class="container" id="logo_section">
    <div class="row flex-nowrap justify-content-between align-items-center" style="padding:0; width:100%!important; margin: 0!important;">
      <div class="col-12 text-center" style="padding:0; width:100%!important;">
        
      <?php 
        $custom_logo_id = get_theme_mod( 'custom_logo' );
        $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
        if ( has_custom_logo() ): 
      ?>
        <?php echo '<img src="'. esc_url( $logo[0] ) .'">'; ?>
      <?php else:?>
        <?php echo '<img src="'. get_stylesheet_directory_uri() . '/images/header.png' .'">'; ?>
      <?php endif; ?>
      </div>
    </div>
  </div>
  
  <header class="blog-header" id="header">
    <div class="container">
      <div class="nav-scroller">
        <nav class="nav d-flex justify-content-between">
            <div class="burger_line">
              <div class="burger">
                <div class="line1"></div>
                <div class="line2"></div>
                <div class="line3"></div> 
              </div>
            </div>
            <?php wp_nav_menu(
                array(
                    'theme_location' => 'top-menu',
                    'menu_class' => 'top-bar'
                )
            ); ?> 
        </nav>
      </div>
    </div>    
  </header>
  
  <script type="">
    $(document).ready(function() {

      $('.burger').click(function(){
        $('#menu-top-menu').toggleClass('active');
      });

      if (window.matchMedia('(max-width: 991px)').matches) {
        $(window).scroll(function() { 
          //change the integers below to match the height of your upper div, which I called
          //banner.  Just add a 1 to the last number.  console.log($(window).scrollTop())
          //to figure out what the scroll position is when exactly you want to fix the nav
          //bar or div or whatever.  I stuck in the console.log for you.  Just remove when
          //you know the position.
          console.log($(window).scrollTop());
          
          if ($(window).scrollTop() > 0) {
            $('header').addClass('fixed-top');
          }

          if ($(window).scrollTop() < 1) {
            $('header').removeClass('fixed-top');
          }
        });
      } else {
        $(window).scroll(function() { 

          console.log($(window).scrollTop());

          if ($(window).scrollTop() > 99) {
            $('header').addClass('fixed-top');
          }

          if ($(window).scrollTop() < 100) {
            $('header').removeClass('fixed-top');
          }
        });
      }


      $(window).resize(function(){
        if (window.matchMedia('(max-width: 991px)').matches) {
          $(window).scroll(function() { 
            console.log($(window).scrollTop());
            
            if ($(window).scrollTop() > 0) {
              $('header').addClass('fixed-top');
            }

            if ($(window).scrollTop() < 1) {
              $('header').removeClass('fixed-top');
            }
          });
        } else {
          $(window).scroll(function() { 

            console.log($(window).scrollTop());

            if ($(window).scrollTop() > 101) {
              $('header').addClass('fixed-top');
            }

            if ($(window).scrollTop() < 102) {
              $('header').removeClass('fixed-top');
            }
          });
        }
      }).resize();


    });
  </script>
