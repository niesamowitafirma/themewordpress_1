<?php get_header(); ?>

<div class="container pt-5 pb-5">

    <h1>Search Results '<?php echo get_search_query();?>'</h1>   
    <?php get_template_part('includes/section', 'searchresults'); ?>
    <?php previous_posts_link(); ?>
    <?php next_posts_link(); ?>

</div>

<?php get_footer(); ?>