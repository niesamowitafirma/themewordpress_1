<?php get_header(); ?>

<div class="container pt-5 pb-5">

    <h1><?php the_title(); ?></h1>
    
    <?php get_template_part('includes/section', 'blogcontent'); ?>

    <ul>
        <li>
            Color: <?php the_field('color'); ?>
        </li>

        <li>
            Registration: <?php the_field('registration'); ?>
        </li>
        
        <?php if(get_post_meta($post->ID, 'Registration', true)):?>

        <li>
            Registration: <?php echo get_post_meta($post->ID, 'Registration', true); ?>
        </li>

        <?php endif; ?>
    </ul>
</div>

<?php get_footer(); ?>