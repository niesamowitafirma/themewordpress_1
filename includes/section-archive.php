
<?php

// The Loop in order to get different display on first post
// As well as display other posts in pairs
if (have_posts() ) :
    $i = 0;
    while (have_posts() ) :
                the_post();
        if ( $i == 0 ) : ?>
           <!-- First Post-->
           <a href="<?php the_permalink(); ?>">
           <div class="jumbotron p-0 p-md-0 rounded-0 text-white row" style="-webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1);
-moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1);
box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1); border: 1px solid #dee2e6!important; min-height: 304px; background-color: white; dipslay: flex; justify-content: space-between; margin: 1px; margin-bottom: 32px;">
                <div class="siema" style="color: black; display: flex; flex: 1; flex-direction: column; padding: 20px; padding-right: 10px;">
                    <h1 class="display-4 font-italic"><?php the_title(); ?></h1>
                    <p class="lead my-3"><?php the_excerpt();?></p>
                </div>
                <div class="blog_img_mid" style="padding:0; height: 100%;">
                    <?php if(has_post_thumbnail()):?>
                        <img width="243" height="304" src="<?php the_post_thumbnail_url('middle'); ?>" class="img-fluid">
                    <?php endif; ?>    
                </div>
            </div>
            </a>
        <?php endif;
        /* Finding the first two posts (if they exist) in order to display them, 
            or to display just one of them */
        if ( $i == 1 || $i == 2 ) : ?>
            <?php if ( $i == 1): ?>
                <!-- Opening a row to keep in it First Pair Of Posts-->
                 <div class="row mb-2">
                    <div class="col-md-6">
                        <a href="<?php the_permalink(); ?>">
                        <div class="row no-gutters border overflow-hidden rounded-0 flex-md-row mb-4 shadow-sm h-md-250 position-relative" style="min-height: 250px; -webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1);
-moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1);
box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1); border: 1px solid #dee2e6!important; background-color: white; color: black;">
                            <div class="siema" style="display: flex; flex: 1; flex-direction: column; padding: 20px; padding-right: 10px;">
                                <!--strong class="d-inline-block mb-2 text-primary">World</strong-->
                                <h3 class="mb-0"><?php the_title(); ?></h3>
                                <div class="mb-1 text-muted">Nov 12</div>
                                <p class="card-text mb-auto"><?php the_excerpt();?></p>
                            </div>
                            <div class="blog_img_sm" style="padding:0; height: 100%;">
                                <?php if(has_post_thumbnail()):?>
                                    <img width="200" height="250" src="<?php the_post_thumbnail_url('smallest'); ?>" class="img-fluid" >
                                <?php endif; ?>    
                            </div>
                        </div>
                        </a>
                    </div>
            <?php elseif ( $i == 2): ?>
                    <div class="col-md-6">
                        <a href="<?php the_permalink(); ?>">        
                        <div class="row no-gutters border overflow-hidden rounded-0 flex-md-row mb-4 shadow-sm h-md-250 position-relative" style="min-height: 250px; -webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1);
-moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1);
box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1); border: 1px solid #dee2e6!important; background-color: white; color: black;">
                            <div class="col p-4 d-flex flex-column position-static">
                                <!--strong class="d-inline-block mb-2 text-primary">World</strong-->
                                <h3 class="mb-0"><?php the_title(); ?></h3>
                                <div class="mb-1 text-muted">Nov 12</div>
                                <p class="card-text mb-auto"><?php the_excerpt();?></p>
                            </div>
                            <div class="blog_img_sm" style="padding:0; height: 100%;">
                                <?php if(has_post_thumbnail()):?>
                                    <img width="200" height="250" src="<?php the_post_thumbnail_url('smallest'); ?>" class="img-fluid" >
                                <?php endif; ?>    
                            </div>
                        </div>
                        </a>
                    </div>
                <!-- Closing a row if there is even number of posts in pair-->    
                </div>
            <?php endif; ?>
        <?php endif;
        /* Finding the second pair of posts (if it exist) in order to display it, 
            or to display just one remaining post */
        if ( $i == 3 || $i == 4 ) : ?>
            <?php if ( $i == 3): ?>
                <!-- Opening a row to keep in it Second Pair Of Posts-->
                <div class="row mb-2">
                    <div class="col-md-6">
                        <a href="<?php the_permalink(); ?>">
                        <div class="row no-gutters border overflow-hidden rounded-0 flex-md-row mb-4 shadow-sm h-md-250 position-relative" style="min-height: 250px; -webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1);
-moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1);
box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1); border: 1px solid #dee2e6!important; background-color: white; color: black;">
                            <div class="col p-4 d-flex flex-column position-static">
                                <!--strong class="d-inline-block mb-2 text-primary">World</strong-->
                                <h3 class="mb-0"><?php the_title(); ?></h3>
                                <div class="mb-1 text-muted">Nov 12</div>
                                <p class="card-text mb-auto"><?php the_excerpt();?></p>
                            </div>
                            <div class="blog_img_sm" style="padding:0; height: 100%;">
                                <?php if(has_post_thumbnail()):?>
                                    <img width="200" height="250" src="<?php the_post_thumbnail_url('smallest'); ?>" class="img-fluid" >
                                <?php endif; ?>    
                            </div>
                        </div>
                        </a>
                    </div>
            <?php elseif ( $i == 4): ?>
                    <div class="col-md-6">
                        <a href="<?php the_permalink(); ?>">
                        <div class="row no-gutters border overflow-hidden rounded-0 flex-md-row mb-4 shadow-sm h-md-250 position-relative" style="min-height: 250px; -webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1);
-moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1);
box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1); border: 1px solid #dee2e6!important; background-color: white; color: black;">
                            <div class="col p-4 d-flex flex-column position-static">
                                <!--strong class="d-inline-block mb-2 text-primary">World</strong-->
                                <h3 class="mb-0"><?php the_title(); ?></h3>
                                <div class="mb-1 text-muted">Nov 12</div>
                                <p class="card-text mb-auto"><?php the_excerpt();?></p>
                            </div>
                            <div class="blog_img_sm" style="padding:0; height: 100%;">
                                <?php if(has_post_thumbnail()):?>
                                    <img width="200" height="250" src="<?php the_post_thumbnail_url('smallest'); ?>" class="img-fluid" >
                                <?php endif; ?>    
                            </div>
                        </div>
                        </a>    
                    </div>
                <!-- Closing a row if there is even number of posts in pair-->    
                </div>
            <?php endif; ?>
        <?php endif; ?>
        <?php $i++;
    endwhile; 

    /* this checks if "$i" is odd*/
    $remainder = $i % 2;
    if ($remainder != 0):
        /* if it is, it means that "row mb-2" is
        Not closed so it has to be closed here  */    
        echo '</div>';
    endif;

endif;?>