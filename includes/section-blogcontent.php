
<?php if(has_post_thumbnail()):?>
    
    <img src="<?php the_post_thumbnail_url('largest'); ?>" class="img-fluid">

<?php endif; ?>
<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

    <h1><?php the_title(); ?></h1>
    
    <p><?php echo get_the_date('d/m/Y'); ?></p>

    <?php the_content(); ?>

    <?php 
    $fname = get_the_author_meta('first_name');
    $lname = get_the_author_meta('last_name');
    ?>

    <p>Created by <?php echo $fname;?> <?php echo $lname; ?></p>

    <?php
    $tags = get_the_tags();
    if ($tags):
    foreach($tags as $tag):?>

        <a href="<?php echo get_tag_link($tag->term_id); ?>">
            <?php echo $tag->name; ?>
        </a>

    <?php endforeach; endif; ?>

    <!--?php comments_template(); ?-->

<?php endwhile; else: endif; ?>
