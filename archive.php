<?php get_header(); ?>

<div class="container pt-5 pb-5">
    <div class="col-lg-3">
        
        <?php if( is_active_sidebar('page-sidebar') ): ?>
            <?php dynamic_sidebar('page-sidebar'); ?>
        <?php endif; ?>

    </div>
    
    <?php get_template_part('includes/section', 'archive'); ?>
    
</div>

<?php get_footer(); ?>