<?php get_header(); ?>

<div class="section" id="post_section">
    <div class="container" id="post_container" style="-webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1);
-moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1);
box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1); border: 1px solid #dee2e6;">

    <?php get_template_part('includes/section', 'blogcontent'); ?>

    </div>
</div>

<?php get_footer(); ?>